# React.js Hiring Project

This is to check your proficiency in react.js Developement. Using ES6 best practises, including unit test with test runners and pushing your code in production earns you bonus points! 

## Problem

You're expected to build a [Fake Twitter](https://giphy.com/gifs/lzQpRfJiKveT1w3ESH/html5). The app shows a feed of all the tweets of people i follow, and a timeline of tweets 
on a particular person page. There will replies, retweet and favorite tweet feature. While creating a profile one should be able to upload photo. Since the gif shows a mobile version of 
twitter, you're expected to take reasonable freedom in using only desktop relevant assests. Still, the final app is expected to be responsive.  

## Acceptance Criteria

- Target 1 (20% score) : Sanity check : After user has logged in they should be able to see all the tweets by that time. This is primarliy test for DB read/write.           
- Target 2 (40% score) : Retweet and Comment: User should be able to send a new tweet, retweet an exsisting tweet and reply to a tweet. These actions should 
- change appropriate counters. This primaraily tests your exposure to React.
- Target 3 (60% score) : In the personal profile; all `favorite` marked tweets should be listed under a tab. This tests both db read/write and react together.
- Target 4 (80% score) : Check for common fallbacks; what screen is shown if login fails, if the're no people one follow. This is to test your logical abilities. 

## Submission  

Create a git repo on gitlab or github, whichever you are more comfortable with, and share the link of the code base.

## Questions

For any questions, please contact saurabh@inbe.com.

## Bonus Points 

1. **+ 5%** : If you have added proper unit test with test runner in the code. Ideally, a `npm run test` should run the tests.
2. **+ 5%** : If you have added proper comments with README for running the codeabse locally and followed standard ES6 conventions
3. **+ 10%** : If deployed the app in production (:means we can intereact with it without going through the codebase) . PLEASE NOTE: This does not remove the requriement of submitting the code base. 
