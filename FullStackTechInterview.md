# Full Stack Hiring Project

This is to check your proficiency in Full Stack Developement. We define Full Stack as being proficient in frontend, api development and database handling. It is expected to have some knowldge of DevOps, atleast comfortable in managing your own deployment. Using ES6 best practises, including unit test with test runners and pushing your code in production earns you bonus points! 

## Problem

You're expected to build a [Fake Slack](https://cdn-images-1.medium.com/max/1600/1*YuxM67ZycijWZwkCZ_aH5Q.gif). The app shows a group chat to which new users can be invited, one to one message to all the available members (no invite needed) and file upload feature with reasonable size limit. There is a search bar which is ideally used to search both users and messages, search for users is a must to have feature, search of messages is a good to have a feature. The ES6 conventions and features should be used and best practise of development is expected to be followed.     

## Acceptance Criteria


- [ ] 20% score : Sanity check : A chat room would be able to send one on one message or can send it in a group. Previously sent messages are available in the chat rooms. 
- [ ] 40% score : Channel creation is required and invite needs to be sent for accessing those channels (Send an email to new user).
- [ ] 50% score : Online/Offline status of the user needs to be maintained. Comments can be made on any post.
- [ ] 70% score : Multiple File upload feature is required with a limit of 1MB. Authenticated Access with email ids as username and passwords.
- [ ] 80% score : Searching for members.


## Submission  

Create a git repo on gitlab or github, whichever you are more comfortable with, and share the link of the code base.

## Questions

For any questions, please contact saurabh@inbe.com.

## Bonus Points 

1. **+ 5%** : If you have added proper unit test with test runner in the code. Ideally, a `npm run test` should run the tests. Test coverage should be ideally greater than 80%.
2. **+ 5%** : If you have added proper code comments with README for running the codeabse locally and followed ES6 production conventions. 
3. **+ 10%** : If you have added proper production logging in the queueing system with file rotate and hosted it on a publically assceable ip, use free credits in AWS, google cloud or digitalOcean . (:hosting on heroku is only half the bonus here) . PLEASE NOTE: This does not remove the requriement of submitting the code base. 






