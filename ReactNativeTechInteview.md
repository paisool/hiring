# React Native Hiring Project

This is to check your proficiency in React Native Developement. Using ES6 best practises, including unit test with test runners and pushing your code in production earns you bonus points! 

## Problem

You're expected to build a [Hiring Tinder](https://cdn-images-1.medium.com/max/1600/1*9l9nVKVVQ19_H7OaEiEdkg.gif). The app shows the profiles of all the applicants registered,
with their details and expectatins. There will be a applicant registration and employer login. Once an applicant has registered they cannot register again. Employer can login and see the profiles
as shown in the gif. 

## Primary Goal

- Target 1 (20% score) : Sanity check : After employer has logged in they should be able to see all the applicants registerd by that time; with correct details. This is primarliy test for DB read/write.           
- Target 2 (40% score) : Naviagtion in the Employer Login: The applicants should be listed as shown in the attached gif and right and left navigtion should be correct. This primaraily test your exposure to React native.
- Target 3 (60% score) : In the hamburger menu; all `favorite` marked applicants should be listed under a tab. This tests both db read/write and react native together.
- Target 4 (80% score) : Check for common fallbacks; what screen is shown to the employeer if the're NO applicants registerd. What happens if the same
applicant wants to register twice. This is to test your logical abilities. 


### Note
The right and left swipe are only for navigation and DO NOT delete the applicant profiles. If you are not comfortable with database read, write you can hardcode the values in the app for this exercise. 


### Submission : 

Create a git repo on gitlab or github which ever you are more comfortable with and share the link of the code base.

## Questions

For questions, please contact saurabh@inbe.com.

## Bonus Points 

1. **+ 5%** : If you have added proper unit test with test runner in the code. 
2. **+ 5%** : If you have added proper comments with README for running the codeabse and followed standard ES6 conventions
3. **+ 10%** : If deployed the app in production (:means we can intereact with it without going through the codebase) . PLEASE NOTE: This does not remove the requriement of submitting the code base. 
